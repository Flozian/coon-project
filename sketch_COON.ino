#include <Servo.h>
#define echoPin 10
#define trigPin 9

Servo servo;
float angle = 0; 
float anglePrec = 0;
long duration; 
float distance = 0;
float distancePrec = 0;
bool montee = true;
bool changeable = true;
long lastChange= 0;
long last15 = 0;

void setup() {
  servo.attach(3);
  servo.write(angle);

  pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
  Serial.begin(9600);

}

void loop() {
  // Clears the trigPin condition
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  duration = pulseIn(echoPin, HIGH);
  
  // Calcul de distance avc les ondes sonores
  distance = duration * 0.034 / 2; 
  
  // Debug de la distance sur le moniteur
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println(" cm");
  delay(20);

  // Si la distance captée est comprise dans ce que l'on souhaite
  if(distance < 35 && distance > 4) {
    // Et si on n'a pas récemment changé d'inclinaison
    if(changeable) {
      if (distancePrec > distance) {
        montee = true;
        lastChange = millis();
      } else {
        montee = false;
        lastChange = millis();
      }
    }

  //Changement pour le booléen, pour savoir si on a changé d'inclinaison il y a longtemps ou pas.
    if((millis() - lastChange) > 200 ){
      changeable = true;
    } else {
      changeable = false;
    }

    if(montee && distance < 20){
      servo.write(100);
    } else if ( !(montee) && distance > 6) {
      servo.write(180);
    }
    distancePrec = distance;
  }
}
